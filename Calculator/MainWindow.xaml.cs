﻿using System;
using System.Data;
using System.Linq;
using System.Media;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            EventManager.RegisterClassHandler(typeof(Button), Button.ClickEvent, new RoutedEventHandler(Button_Click));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender as Button != memoryButton)
                calculatedExpression.Text = string.Empty;
        }

        // Ввод открывающей скобки
        private void BracketOpenButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            char lastChar = mainText.Text.LastOrDefault();
            char[] wrongEndSymbols = new char[] { ')', '!', 'd', '.' };

            // Если выражение состоит из единственной цифры 0, то заменить открывающей скобкой
            if (mainText.Text == "0")
                mainText.Text = button.Content.ToString();

            // Добавить знак (
            else if (!wrongEndSymbols.Contains(lastChar)
                    && !char.IsDigit(lastChar))
                mainText.Text += button.Content;

            // Добавить знак *(
            else if (lastChar != '.'
                     && lastChar != 'd')
                mainText.Text += "*" + button.Content;

            // Некорректный ввод
            else
                SystemSounds.Beep.Play();
        }

        // Ввод закрывающей скобки
        private void BracketCloseButton_Click(object sender, RoutedEventArgs e)
        {
            string text = mainText.Text;
            char lastChar = text.LastOrDefault();

            // Добавить символ ')' если посл. символ - это цифра или ')' или '!'
            // и кол-во закрывающих не превышает кол-во открывающих
            if ((char.IsDigit(lastChar)
                || lastChar == ')'
                || lastChar == '!')
                && text.Count(x => x == ')') < text.Count(x => x == '('))
                mainText.Text += (sender as Button).Content;

            // Некорректный ввод
            else
                SystemSounds.Beep.Play();
        }

        // Ввод цифры
        private void DigitButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            string text = mainText.Text;

            // если посл. число 0, заменить его введенной цифрой
            if (text.Split('+', '-', '*', '/', 'd').Last() == "0")
                mainText.Text = text.Substring(0, text.Length - 1) + button.Content;

            // добавить цифру к выражению
            else if (text.LastOrDefault() != ')'
                    && text.LastOrDefault() != '!')
                mainText.Text += button.Content;

            // иначе добавить знак '*' и цифру
            else
                mainText.Text += "*" + button.Content;
        }

        // Ввод AllClear
        private void AllClearButton_Click(object sender, RoutedEventArgs e)
        {
            // Заменить выражение нулем
            mainText.Text = "0";
        }

        // Ввод Delete (удалить последний символ)
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            string expression = mainText.Text;

            // если выражение завершается на ru( или rd(
            if (expression.Length >= 3
                && (expression.Substring(expression.Length - 3, 3) == "ru("
                    || expression.Substring(expression.Length - 3, 3) == "rd("))
                // удалить последние три символа
                mainText.Text = expression.Substring(0, expression.Length - 3);

            // если выражение завершается на sqrt(
            else if (expression.Length >= 5
                    && expression.Substring(expression.Length - 5, 5) == "sqrt(")
                // удалить последние пять символов
                mainText.Text = expression.Substring(0, expression.Length - 5);

            // иначе удалить последний символ
            else
                mainText.Text = expression.Substring(0, expression.Length - 1);

            // Если выражение оказалось пустым, заменить на ноль
            if (mainText.Text.Length == 0)
                mainText.Text = "0";
        }

        // Ввод знаков: деление, степень, умножение, минус, плюс 
        private void AriphmeticsButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            char lastChar = mainText.Text.LastOrDefault();

            // Ввод возможен, если последний символ ')', цифра или '!'
            if (lastChar == ')'
                || char.IsDigit(lastChar)
                || lastChar == '!'
                || (button.Content.ToString() == "-" && lastChar == '(')) // Для выражения (-
                mainText.Text += button.Content.ToString()
                                    .Replace("x", "")
                                    .Replace("y", "");
            // Некорректный ввод
            else
                SystemSounds.Beep.Play();
        }

        // Ввод знаков: ru(, rd(, sqrt(
        private void ComplexButton_Click(object sender, RoutedEventArgs e)
        {
            string text = mainText.Text;
            char[] allowedEndSymbols = new char[] { '(', '+', '-', '*', '/', '^' };

            string buttonText = (sender as Button).Content.ToString()
                                    .Replace("√x", "sqrt(")
                                    .Replace("ru", "ru(")
                                    .Replace("rd", "rd(");

            // если выражение равно 0, то заменить на нужный знак
            if (text == "0")
                mainText.Text = buttonText;

            // иначе прибавить нужный знак
            else if (allowedEndSymbols.Contains(text.LastOrDefault()))
                mainText.Text += buttonText;

            // умножить
            else
                mainText.Text += "*" + buttonText;
        }

        // Ввод знаков '.', 'd' и '!' после целого числа
        private void PointDFactorialButton_Click(object sender, RoutedEventArgs e)
        {
            // Если текст завершается целым числом 
            if (char.IsDigit(mainText.Text.LastOrDefault())
                && IsDigitsOnly(mainText.Text.Split('+', '-', '*', '/', '(', ')', '^').Last()))

                mainText.Text += (sender as Button)
                    .Content
                    .ToString()
                    .Replace("x", "");
            else
                SystemSounds.Beep.Play(); // Некорректный ввод
        }

        private bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
                if (c < '0' || c > '9')
                    return false;

            return true;
        }

        // Подсчет результата после нажатия на '='
        private async void ResultButton_Click(object sender, RoutedEventArgs e)
        {
            IsEnabled = false;

            string expression = mainText.Text;

            try
            {
                // Выражения корректное, если последний символ: ')', цифра или факториал 
                // и если кол-во открывающих и закрывающих скобок совпадает
                if ((expression.LastOrDefault() == ')'
                    || char.IsDigit(expression.LastOrDefault())
                    || expression.LastOrDefault() == '!')
                    && expression.Count(x => x == '(') == expression.Count(x => x == ')'))
                {
                    calculatedExpression.Text = expression + '=';
                    mainText.Text = Calculate(mainText.Text);

                    if (mainText.Text == "∞")
                        throw new DivideByZeroException();

                    // Удалить верхние три (выражение, результат, разделитель)
                    if (historyListBox.Items.Count == 15)
                    {
                        historyListBox.Items.RemoveAt(0);
                        historyListBox.Items.RemoveAt(1);
                        historyListBox.Items.RemoveAt(2);
                    }

                    // Добавить три (выражение, результат, разделитель)
                    {
                        ListBoxItem expressionItem = new ListBoxItem
                        {
                            Content = expression,
                            Cursor = Cursors.Hand
                        };
                        expressionItem.PreviewMouseLeftButtonDown += HistoryMouseDown;

                        ListBoxItem resultItem = new ListBoxItem
                        {
                            Content = mainText.Text,
                            Cursor = Cursors.Hand,
                            Foreground = Brushes.Green
                        };
                        resultItem.PreviewMouseLeftButtonDown += HistoryMouseDown;

                        historyListBox.Items.Add(expressionItem);
                        historyListBox.Items.Add(resultItem);
                        historyListBox.Items.Add(new Separator());
                    }
                }
                else if (expression.Count(x => x == '(') != expression.Count(x => x == ')'))
                    throw new Exception("Закрыты не все открытые скобки.");
                else
                    throw new Exception($"Выражение не может заканчиваться на '{expression.LastOrDefault()}'.");
            }
            catch (Exception ex)
            {
                SystemSounds.Beep.Play();

                errorText.Text = ex.Message;
                await Task.Delay(2000);
                errorText.Text = string.Empty;

                mainText.Text = expression;
            }

            IsEnabled = true;
        }

        private void HistoryMouseDown(object sender, RoutedEventArgs e)
        {
            ListBoxItem listBoxItem = sender as ListBoxItem;

            mainText.Text = listBoxItem.Content.ToString();

            if (listBoxItem.Foreground == Brushes.Green)
                calculatedExpression.Text =
                    (historyListBox.Items.GetItemAt(historyListBox.Items.IndexOf(listBoxItem) - 1) as ListBoxItem).Content.ToString() + '=';
            else
                calculatedExpression.Text = string.Empty;
        }

        // Вычисляет рекурсивно по скобкам 
        private string Calculate(string expression)
        {
            if (expression.Contains('(')
                || expression.Contains(')'))
            {
                int bracketOpenIndex = -1;
                int bracketOpenCount = 0;
                int bracketCloseCount = 0;
                string bracketContent = string.Empty;

                for (int i = 0; i < expression.Length; i++)
                {
                    if (expression[i] == '(')
                    {
                        if (bracketOpenIndex == -1)
                            bracketOpenIndex = i;

                        bracketOpenCount++;
                    }

                    if (expression[i] == ')')
                    {
                        bracketCloseCount++;

                        if (bracketCloseCount == bracketOpenCount)
                        {
                            // if ru(
                            // beforeBracketOpenPart + (ru(calculate(bracketPart)) or rd(calculate(bracketPart))) + afterBracketClosePart
                            if (bracketOpenIndex >= 2 && expression.Substring(bracketOpenIndex - 2, 3) == "ru(")
                            {
                                bracketContent = expression.Substring(bracketOpenIndex + 1, i - 1 - bracketOpenIndex);
                                bracketContent = IsDecimal(bracketContent) ? bracketContent  : Calculate(bracketContent);

                                expression = expression
                                    .Replace(expression.Substring(bracketOpenIndex - 2, i - bracketOpenIndex + 3).ToString(),
                                            '[' + Math.Ceiling(Convert.ToDouble(bracketContent.Replace(".", ","))).ToString().Replace(",", ".") + ']');
                            }
                            // if rd(
                            else if (bracketOpenIndex >= 2 && expression.Substring(bracketOpenIndex - 2, 3) == "rd(")
                            {
                                bracketContent = expression.Substring(bracketOpenIndex + 1, i - 1 - bracketOpenIndex);
                                bracketContent = IsDecimal(bracketContent) ? bracketContent : Calculate(bracketContent);

                                expression = expression
                                    .Replace(expression.Substring(bracketOpenIndex - 2, i - bracketOpenIndex + 3).ToString(),
                                            '[' + Math.Floor(Convert.ToDouble(bracketContent.Replace(".", ","))).ToString().Replace(",", ".") + ']');
                            }
                            // if sqrt(
                            // beforeBracketOpenPart + (sqrt(calculate(bracketPart))) + afterBracketClosePart
                            else if (bracketOpenIndex >= 4 && expression.Substring(bracketOpenIndex - 4, 5) == "sqrt(")
                            {
                                bracketContent = expression.Substring(bracketOpenIndex + 1, i - 1 - bracketOpenIndex);
                                bracketContent = IsDecimal(bracketContent) ? bracketContent : Calculate(bracketContent);

                                expression = expression
                                    .Replace(expression.Substring(bracketOpenIndex - 4, i - bracketOpenIndex + 5).ToString(),
                                            '[' + Math.Sqrt(Convert.ToDouble(bracketContent.Replace(".", ","))).ToString().Replace(",", ".") + ']');
                            }
                            // if ()
                            else
                            {
                                bracketContent = expression.Substring(bracketOpenIndex + 1, i - bracketOpenIndex - 1);
                                bracketContent = IsDecimal(bracketContent) ? bracketContent : Calculate(bracketContent);

                                expression = expression
                                    .Replace(expression.Substring(bracketOpenIndex, i - bracketOpenIndex + 1).ToString(),
                                            '[' + bracketContent + ']');
                            }

                            // drop values
                            bracketOpenIndex = -1;
                            bracketOpenCount = 0;
                            bracketCloseCount = 0;
                        }
                    }
                }

                return Calculate(expression);
            }
            else
            {
                expression = expression
                                .Replace("[", "(")
                                .Replace("]", ")");

                string finalExpression = string.Empty;
                string[] expressionParts = new string[expression.Length]; // expression.Split('+', '-', '*', '/');
                char?[] ariphmeticChars = new char?[expression.Length]; //new char[expression.Length];

                string expressionPart = string.Empty;
                int expressionPartsIndex = 0;
                for (int i = 0; i < expression.Length; i++)
                {
                    // Если не + - * / или отрицательный знак и не конец выражения
                    if (((expression[i] != '+' && expression[i] != '-' && expression[i] != '*' && expression[i] != '/')
                        || (expression[i] == '-' && i > 0 && expression[i - 1] == '(')
                        || (i==0 && expression[i] == '-'))
                        && i != expression.Length - 1)
                    {
                        expressionPart += expression[i];
                    }
                    else
                    {
                        if (i == expression.Length - 1)
                            expressionPart += expression[i];

                        expressionParts[expressionPartsIndex] = expressionPart;
                        expressionPart = string.Empty;

                        if (expression[i] == '+' || expression[i] == '-' || expression[i] == '*' || expression[i] == '/')
                            ariphmeticChars[expressionPartsIndex] = expression[i];

                        expressionPartsIndex++;
                    }
                }

                // Каждый кусок вычислить и прибавить знак
                for (int i = 0; i < expressionParts.Length; i++)
                {
                    if (string.IsNullOrEmpty(expressionParts[i]))
                        break;

                    // Подсчет факториала
                    if (expressionParts[i].Contains('!'))
                    {
                        if (Convert.ToInt32(expressionParts[i].Replace("!", "")) > 20)
                            throw new Exception("Макс. вычисляемое число фактриала: 20");
                        else
                            finalExpression += Factorial(Convert.ToInt32(expressionParts[i].Replace("!", "")));
                    }
                    // Степень
                    else if (expressionParts[i].Contains('^'))
                    {
                        finalExpression += Math
                            .Pow(Convert
                                    .ToDouble(expressionParts[i]
                                                .Split('^')[0]
                                                .Replace(".", ",")
                                                .Replace("(", "")
                                                .Replace(")", "")
                                                ),
                                    Convert
                                    .ToDouble(expressionParts[i]
                                                .Split('^')[1]
                                                .Replace(".", ",")
                                                .Replace("(", "")
                                                .Replace(")", "")
                                                )
                                );
                    }
                    // Бросок кубиков
                    else if (expressionParts[i].Contains('d'))
                    {
                        if (expressionParts[i].Split('d')[0] == "0"
                            || expressionParts[i].Split('d')[1] == "0")
                            throw new Exception("Некорректные числа для броска кубиков.");
                        else
                            finalExpression += Cube(Convert.ToInt32(expressionParts[i].Split('d')[0]), Convert.ToInt32(expressionParts[i].Split('d')[1]));
                    }
                    // Обычное число
                    else
                    {
                        finalExpression += expressionParts[i];
                    }

                    // В конце прибавим знак
                    if (ariphmeticChars[i].HasValue)
                        finalExpression += ariphmeticChars[i];
                }

                return new DataTable()
                    .Compute(finalExpression
                                .Replace(",", "."), "")
                    .ToString()
                        .Replace(",", ".");
            }
        }

        static bool IsDecimal(string expression)
        {
            decimal value;

            return decimal.TryParse(expression.Replace(".", ","), out value);             
        }

        static int Factorial(int x)
        {
            if (x == 0)            
                return 1;
            
            return x * Factorial(x - 1);            
        }

        static int Cube(int cubesCount, int edgesCount)
        {
            int finalSum = 0;

            // cubesCount раз бросить кубик
            for (int i = 0; i < cubesCount; i++)            
                finalSum += new Random().Next(1, edgesCount);            

            return finalSum;
        }

        private void MemoryButton_Click(object sender, RoutedEventArgs e)
        {
            if (mainTabControl.SelectedIndex == 0)
            {
                mainTabControl.SelectedIndex = 1;
                memoryButton.Content = "X";
            }
            else
            {
                mainTabControl.SelectedIndex = 0;
                memoryButton.Content = "M";
            }
        }
    }
}
